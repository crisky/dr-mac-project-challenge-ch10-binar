import fs from 'fs'
import { join } from 'path'
import matter from 'gray-matter'

const gamelistsDirectory = join(process.cwd(), '_gamelists')

export function getGamelistSlugs() {
  return fs.readdirSync(gamelistsDirectory)
}

export function getGamelistBySlug(slug, fields = []) {
  const realSlug = slug.replace(/\.md$/, '')
  const fullPath = join(gamelistsDirectory, `${realSlug}.md`)
  const fileContents = fs.readFileSync(fullPath, 'utf8')
  const { data, content } = matter(fileContents)

  const items = {}

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug
    }
    if (field === 'content') {
      items[field] = content
    }

    if (typeof data[field] !== 'undefined') {
      items[field] = data[field]
    }
  })

  return items
}

export function getAllGamelists(fields = []) {
  const slugs = getGamelistSlugs()
  const gamelists = slugs
    .map((slug) => getGamelistBySlug(slug, fields))
  return gamelists
}
