//Game Page
import React, { Component, Fragment } from "react";
import batu from '../public/assets/batu.png'
import kertas from '../public/assets/kertas.png'
import gunting from '../public/assets/gunting.png'
import refresh from '../public/assets/refresh.png'
// import NavbarTop from '../components/NavbarTop'
// import 'bootstrap/dist/css/bootstrap.min.css'
import styles from '../styles/Game.module.css'
import Image from 'next/image'
// let play = () => console.log('batu')
// let playGunting = () => console.log('gunting')
// let playKertas = () => console.log('kertas')

let resultName = 'result';


class Game extends Component{

    constructor(props) {
        super(props);
        this.state = {boxColor: '',boxColor2:'',boxColor3:''};
        this.state = {result: "VS"}
        
        
     
      }

      confirmation(){
        window.confirm('Apakah ingin mengulang permainan ?');
      }


      refresh() {
        this.setState(() => ({
            boxColor:  '',
            boxColor2:  '',
            boxColor3:  '',
            boxColor4:  '',
            boxColor5:  '',
            boxColor6:  '',
            result: 'VS',
            clickEvent: '', 
            clickEvent2: '' ,
            clickEvent3: '',
            
          }));
          resultName= 'result'
      }
    
    playerChoose = (player) => {
                
        const comShuffle = ['batu', 'kertas', 'gunting'];
        this.comChoose = comShuffle[Math.floor(Math.random() * comShuffle.length)];

        if(this.comChoose === 'batu'){
            this.setState(() => ({
                boxColor4:  'gold',
                
              }));
        }else if(this.comChoose === 'kertas'){
            this.setState(() => ({
                boxColor5:  'gold',
                
              }));
        }else{
            this.setState(() => ({
                boxColor6:  'gold',
               
              }));
        }

        if(player === 'batu'){
            this.setState(() => ({
                boxColor:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }else if (player === 'kertas'){
            this.setState(() => ({
                boxColor2:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }else{
            this.setState(() => ({
                boxColor3:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }

        
        if(player === this.comChoose){
            this.setState({ result: "draw"});
            resultName = 'draw'

            
          
        }else if (
            (player === "batu" && this.comChoose === "gunting") || (player === "gunting" && this.comChoose === "kertas") || (player === "kertas" && this.comChoose === "batu")
            ){
        
                this.setState({ result: "Pwin"}); 
                resultName = 'pwin'
                
        }else{
            
            this.setState({ result: "Com Win"});
            resultName = 'cwin'
   
        }
        

      };
    
      
    render(){

        return (
  

            <Fragment>
                {/* <>
                <NavbarTop sticky="top" />                 
                </> */}

                                 
                        
                <div className={styles.containerGame}>
                    <div className={styles.wrapper,styles.wrapper3}>
                        <div className={styles.user}>

                            <div className={styles.box4}>
                                <h3>PLAYER</h3>
                            </div>

                            <div onClick={() => {this.playerChoose("batu")}} className={styles.box3} style={{ backgroundColor: this.state.boxColor, pointerEvents: this.state.clickEvent }} id="batu-p">
                                <Image className={styles.suit} src={batu} alt="Batu" />
                            </div>
                            
                            <div onClick={() => {this.playerChoose("kertas")}}  className={styles.box3} style={{ backgroundColor: this.state.boxColor2, pointerEvents: this.state.clickEvent2 }} id="kertas-p" >
                                <Image className={styles.suit} src={kertas} alt="kertas" />
                            </div>

                            <div onClick={() => {this.playerChoose("gunting")}} className={styles.box3} style={{ backgroundColor: this.state.boxColor3, pointerEvents: this.state.clickEvent3 }} id="gunting-p">
                                <Image className={styles.suit} src={gunting} alt="gunting" />    
                            </div>
                        </div>

                        <div className={styles.mid}>
                            <div className={styles.result} id="hasil" >{ this.state.result }</div>
                        </div> 

                        <div className={styles.com}>
                            <div className={styles.box4}>
                            <h3>COM</h3>
                            </div>
                            
                            <div  className={styles.box3} id="batu-c" style={{ backgroundColor: this.state.boxColor4 }} value='batu'>
                                <Image className={styles.suit} src={batu} alt="Batu" />
                            </div>

                            <div  className={styles.box3} style={{ backgroundColor: this.state.boxColor5 }} id="kertas-c" value='kertas'>
                                <Image className={styles.suit} src={kertas} alt="kertas" />
                            </div>

                            <div  className={styles.box3} style={{ backgroundColor: this.state.boxColor6 }} id="gunting-c" value='gunting'>
                                <Image className={styles.suit} src={gunting} alt="gunting" /> 
                            </div>   

                            
                        </div>
                    </div>
                </div> 

                <div className={styles.containerGame}>
                    <div className={styles.wrapper, styles.wrapper3}>
                        <div className={styles.refresh}>
                            <div className={styles.box2} id="refresh">
                                <Image className={styles.refBtn} onClick={() => {this.refresh()}} src={refresh} alt="gunting" />
                            </div>  
                        </div>   
                    </div>
                </div>

            </Fragment>
            
        )
    }
}

export default Game;
