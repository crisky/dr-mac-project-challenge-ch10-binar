import { useRouter } from "next/router"
import ErrorPage from 'next/error'
import Layout from '../../components/gamelist/layout'
import Container from "../../components/container"
import Head from "next/head"
import GamelistHeader from "../../components/gamelist/header"
import GamelistBody from "../../components/gamelist/body"
import { getAllGamelists, getGamelistBySlug } from "../../libs/api"
import markdownToHtml from "../../libs/markdownToHtml"

export default function GameDetail({ gamelist, moreGames, preview }) {
    const router = useRouter()
    if (!router.isFallback && !gamelist?.slug) {
        return <ErrorPage statusCode={404} />
    }
    return (
        <Layout preview={preview}>
            <Container>
                {router.isFallback ? (
                    <h1>Loading...</h1>
                ) : (
                    <>
                        <article className="mb-32">
                        <Head>
                            <title>
                                {gamelist.title} | DR MAC Game List
                            </title>
                        </Head>
                        <GamelistHeader
                        title={gamelist.title}
                        coverImage={gamelist.coverImage}
                        />
                        <GamelistBody content={gamelist.content} />
                        <a href="/game">Play Game</a>
                        </article>
                        </>
                )}
            </Container>
        </Layout>
    )
}

export async function getStaticProps({ params }) {
    const gamelist = getGamelistBySlug(params.slug, [
        'title',
        'slug',
        'content',
        'ogImage',
        'coverImage'
    ])
    const content = await markdownToHtml(gamelist.content || '')

    return {
        props: {
            gamelist: {
                ...gamelist,
                content,
            },
        },
    }
}

export async function getStaticPaths() {
    const gamelists = getAllGamelists(['slug'])

    return {
        paths: gamelists.map((gamelist) => {
            return {
                params: {
                    slug: gamelist.slug,
                },
            }
        }),
        fallback: false,
    }
}