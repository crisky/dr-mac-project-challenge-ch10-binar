import Head from 'next/head'
import Container from '../../components/container'
import Layout from '../../components/gamelist/layout'
import HeroGame from '../../components/gamelist/hero-game'
import MoreGames from '../../components/gamelist/more-games'
import { getAllGamelists } from '../../libs/api'

export default function Gamelist({ allGamelists }) {
    const heroGame = allGamelists[0]
    const moreGames = allGamelists.slice(1)
    return (
        <>
            <Layout>
                <Head>
                    <title>DR MAC Game List</title>
                </Head>
                <Container>
                    {heroGame && (
                        <HeroGame
                            title={heroGame.title}
                            coverImage={heroGame.coverImage}
                            slug={heroGame.slug}
                            excerpt={heroGame.excerpt}
                        />
                    )}
                    {moreGames.length > 0 && <MoreGames games={moreGames}/>}
                </Container>
            </Layout>
            </>
    )
}

export async function getStaticProps() {
    const allGamelists = getAllGamelists([
        'title',
        'slug',
        'coverImage',
        'excerpt',
    ])

    return {
        props: { allGamelists },
    }
}