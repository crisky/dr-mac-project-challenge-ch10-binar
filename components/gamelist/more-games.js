import GamePreview from "./game-preview"

export default function MoreGames({ games }) {
    return (
        <section>
            <h2 className="mb-8 text-6xl md:text-7xl font-bold tracking-tighter leading-tight">
                More Games
            </h2>
            <div className="grid grid-cols-1 md:grid-cols-2 md:gap-x-16 lg:gap-x-32 gap-y-20 md:gap-y-32 mb-32">
                {games.map((game) => (
                    <GamePreview
                        key={game.slug}
                        title={game.title}
                        subtitle={game.subtitle}
                        coverImage={game.coverImage}
                        slug={game.slug}
                        excerpt={game.excerpt}
                    />
                ))}
            </div>
        </section>
    )
}