import CoverImage from './cover-image'
import Link from 'next/link'
import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function GamePreview({
  title,
  subtitle,
  coverImage,
  excerpt,
  slug,
}) {
  return (
    <>
{/*       
      <h3 className="text-3xl mb-3 leading-snug">
        <Link href={`/gamelist/${slug}`}>
          <a className="hover:underline">{title}</a>
        </Link>
      </h3>
      <p className="text-lg leading-relaxed mb-4">{excerpt}</p> */}


      <Card className="my-3" style={{ height: 350, width: 300 }}>
        <CoverImage
          slug={slug}
          title={title}
          src={coverImage}
          top
          width="100%"
          height="40%"
        />
        <CardBody>
          <CardTitle tag="h5">
            {title}
          </CardTitle>
          <CardSubtitle
            className="mb-2 text-warning"
            tag="h6"
          >
            {subtitle}
          </CardSubtitle>
          <CardText>
            {excerpt}
          </CardText>
          <Link href={`/gamelist/${slug}`}>
            <Button className="position-absolute bottom-0 end-0 mx-3 my-3">
              Lihat Games
            </Button>
          </Link>
        </CardBody>
      </Card>
    </>
  )
}
