import CoverImage from "./cover-image";
import Link from "next/link";

export default function HeroGame({
    title,
    coverImage,
    excerpt,
    slug
}) {
    return (
        <section>
            <div className="mb-8 md:mb-16">
                <CoverImage
                    title={title}
                    src={coverImage}
                    slug={slug}
                    height={620}
                    width={1240}
                />
            </div>
            <div className="md:grid md:grid-cols-2 md:gap-x-16 lg:gap-x-8 mb-20 md:mb-28">
                <div>
                    <h3 className="mb-4 text-4xl lg:text-6xl leading-tight">
                        <Link href={`/gamelist/${slug}`}>
                            <a className="hover:underline">{title}</a>
                        </Link>
                    </h3>
                </div>
                <div>
                    <p className="text-lg leading-relaxed mb-4">{excerpt}</p>
                </div>
            </div>
        </section>
    )
}