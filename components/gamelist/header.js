import CoverImage from './cover-image'
import GamelistTitle from './title'

export default function GamelistHeader({ title, coverImage }) {
  return (
    <>
      <GamelistTitle>{title}</GamelistTitle>
      <div className="mb-8 md:mb-16 sm:mx-0">
        <CoverImage title={title} src={coverImage} height={620} width={1240} />
      </div>
    </>
  )
}
