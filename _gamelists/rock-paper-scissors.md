---
title: "Rock Paper Scissors"
subtitle: "Brand New Game!"
coverImage: '/assets/gamelist/1191457.jpg'
ogImage: '/assets/gamelist/1191457.jpg'
categories: "Arcade"
excerpt: "Traditional Japanese rock paper game with computers."
---

Kamu bisa memainkan permainan gunting batu kertas yang biasa dimainkan saat anak-anak secara online. Ayo kita kembali ke masa kecil kita!